'''
import files and API
'''
import json
from tweepy import Stream
from tweepy.streaming import StreamListener
import tweet
import couchdb

twitterdb = couchdb.Server('http://115.146.84.167:5984/')['tweets_mel']  # database address


class MyListener(StreamListener):

    def on_data(self, data):
        try:
            decoded = json.loads(data)
            twitterdb.save(decoded)  # save to the couchdb
            return True
        except BaseException as e:
            print("Error on_data: %s" % str(e))
        return True

    def on_error(self, status):
        print(status)
        return True


twitter_stream = Stream(tweet.auth, MyListener())  # start the Twitter listener
twitter_stream.filter(locations=[143.64, -38.85, 145.5, -37.35])  # location of Melbourne
