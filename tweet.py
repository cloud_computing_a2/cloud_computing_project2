'''
import files and APIs
'''
import tweepy
from tweepy import OAuthHandler
import json

'''
twitter authentication
'''
consumer_key = 'consumer_key'
consumer_secret = 'consumer_secret'
access_token = 'access_token'
access_secret = 'access_secret'

auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
'''
call tweepy
'''
api = tweepy.API(auth)

