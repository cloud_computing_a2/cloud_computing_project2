'''
import files and APIs
'''
import json
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import couchdb
'''
connect to the server
'''
couch = couchdb.Server('http://xx.xx.xx.xx:5984/') 
db = couch['tweets_sydeny']
raw_data = ''
retweeted = False
i = 0  # count the total number
pos = 0  # count positive
neu = 0  # count neutral
neg = 0  # count negative
t = 0  # time zone
time = ''
timeblock = [[0 for i in range(3)] for i in range(24)]
analyzer = SentimentIntensityAnalyzer()
for tweet in db:
    try:
        data = json.loads(tweet)
        time = data['doc']['created_at'] # divide tweets by time 
        raw_data = data['doc']['text']
        i = i + 1
        vs = analyzer.polarity_scores(raw_data)
        if vs['compound'] > 0:
            pos = pos + 1
            timeblock[int(time[11:13])][0] += 1
        elif vs['compound'] == 0:
            neu = neu + 1
            timeblock[int(time[11:13])][1] += 1
        else:
            neg = neg + 1
            timeblock[int(time[11:13])][2] += 1

    except (ValueError, KeyError):
        continue
for j in range(24): # change time zone
    if j < 10:
        t = j + 14 
    else:
        t = j - 10
'''
print results
'''
    if 0 != timeblock[t][0] + timeblock[t][1] + timeblock[t][2]:
        print('time:                   ', j, ":00 --", j + 1, ":00")
        print('positive sentiment rate:',
              '%.2f%%' % (timeblock[t][0] / (timeblock[t][0] + timeblock[t][1] + timeblock[t][2]) * 100))
        print('positive sentiment     :', timeblock[t][0])
        print('neutral sentiment      :', timeblock[t][1])
        print('negative sentiment     :', timeblock[t][2])
        print('total:                  ', timeblock[t][0] + timeblock[t][1] + timeblock[t][2])
        print("----\n")

#print(timeblock) 
#print(pos, neu, neg)
print(i)
